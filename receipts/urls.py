from django.urls import path
from receipts.views import (
    AccountListView,
    AccountCreateView,
    ExpenseCategoryListView,
    ExpenseCategoryCreateView,
    ReceiptListView,
    ReceiptCreateView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="categories_list",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_categories",
    ),
]
